def read_data():
	with open('rainfall.csv', 'r') as rain:
		header = rain.readline().split(',')
		header[2] = header[2].strip()
		data = []
		for line in rain:
			row = line.split(',')
			data.append({header[0]:int(row[0]), header[1]: int(row[1]), header[2]: float(row[2].strip())})
	rain.closed
	return data

def dates(data, start=None, end=None):
	if start == None:
		start = -10000
	if end == None:
		end = 10000
	datamod = [item for item in data if start <= item.get('year') <= end]
	return datamod

def paginate(data, offset=None, limit=None):
	if offset == None:
		offset = 0
	if limit == None:
		limit = len(data)
	return data[offset:min(offset + limit, len(data))]
